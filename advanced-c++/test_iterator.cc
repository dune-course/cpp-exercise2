#include <cstdlib>
#include <iostream>
#include <complex>
#include <iomanip>
#include <algorithm>

#include "nummatrix.h"

using namespace std;


//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    // define matrix
    //=====================

    // double 4x3
    NumMatrixClass<double> A(4,3);
    double start1 = 2.0;
    for (int i=0; i<A.Rows(); ++i)
      for (int j=0; j<A.Cols(); ++j)
        A(i,j) = start1 + i + j;


	// (1) Iterate over matrix entries using explicit iterator types
	// Extract types of row/column iterators; you will have to specify those as typedef's in your matrix class!
	typedef NumMatrixClass<double>::ConstRowIt RIt;
	typedef NumMatrixClass<double>::ConstColIt CIt;
	// Use methods begin() and end() to iterate over the matrix rows
	for(RIt rit = A.begin(); rit != A.end(); ++rit)
	{
	  // Since the rows are std::vector's, they also provide begin() and end() for iterating!
	  for(CIt cit = rit->begin(); cit != rit->end(); ++cit)
	  {
			std::cout << *cit << " ";
	  }
	  std::cout << std::endl;
	}

	std::cout << std::endl;

	// (2) Iterate over matrix using types implicitly obtained by 'auto' keyword
	// ...

	std::cout << std::endl;

	// (3) Use the range-based for loop for an even more readable code
	// ...

	std::cout << std::endl;

	// (4) Use lamda functions instead to implement the double loop
	// ...

   return (0);
}
