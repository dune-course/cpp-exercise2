#include <cstdlib>
#include <iostream>
#include <complex>

#include "nummatrix.h"

int main(int argc, char **argv)
{
    // double 6x4
    NumMatrixClass<double> A(6,4,0.0);
    for (int i=0; i<A.Cols(); ++i)
        A(i,i) = 2.1;
    for (int i=0; i<A.Cols()-1; ++i)
        A(i+1,i) = A(i,i+1) = -1.0;
    A.Print();

    // int 6x4
    NumMatrixClass<int> B(6,4,0);
    for (int i=0; i<B.Cols(); ++i)
        B(i,i) = 2;
    for (int i=0; i<B.Cols()-1; ++i)
        B(i+1,i) = B(i,i) = -1;
    B.Print();

    // double 6x4
    NumMatrixClass<double> C(6,4);

    // test operator+ with type promotion traits
    std::cout << "========= testing operator+ with type promotion traits =========" <<
            std::endl << std::endl;
    C = A + B;
    C.Print();

    C = B + A;
    C.Print();

    return (0);
}
