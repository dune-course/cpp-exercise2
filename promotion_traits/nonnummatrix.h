/**
 *  Templated non-numeric MatrixClass
 */

#include <vector>
#include <iostream>
#include <iomanip>

template<class T>
class MatrixClass
{
  public:

    // Set number of matrix rows and columns and 
    // initialize matrix elements with a given value
    void Resize(int numRows, int numCols, const T &value=T() );
    
    // Access matrix element at position (i,j)
    T &operator()(int i, int j);
    T operator()(int i, int j) const;
    
    // Output matrix content
    void Print() const;
    
    // Returns number of matrix raws
    int Rows() const 
    {
        return numRows_;
    }
    
    // Returns number of matrix columns
    int Cols() const
    {
        return numCols_;
    }

    // Constructors
    
    MatrixClass(int numRows=0, int numCols=0, const T &value=T() ) :
      a_(numRows), numRows_(numRows), numCols_(numCols)
    {
      if ( ((numCols==0) && (numRows!=0)) || ((numCols!=0) && (numRows==0)) )
      {
        numRows_=0;
        numCols_=0;
        a_.resize(numRows_);
      }
      else
      {
          for (int i=0;i<numRows_;++i)
              a_[i].resize(numCols_);
      }
      for (int i=0;i<numRows;++i)
      {
          for (int j=0;j<numCols;++j)
              a_[i][j]=value;
      }
    }

    // row iterators
    typedef typename std::vector<std::vector<T> >::iterator RowIterator;
    typedef typename std::vector<std::vector<T> >::const_iterator ConstRowIterator;

    // methods returning row iterators
    RowIterator begin()
    {
        return this->a_.begin();
    }
    RowIterator end()
    {
        return this->a_.end();
    }
    ConstRowIterator begin() const
    {
        return this->a_.begin();
    }
    ConstRowIterator end() const
    {
        return this->a_.end();
    }

    // column iterators
    typedef typename std::vector<T>::iterator ColIterator;
    typedef typename std::vector<T>::const_iterator ConstColIterator;

  protected:
    // matrix elements
    std::vector<std::vector<T> > a_;
    // number of rows
    int numRows_;
    //number of columns
    int numCols_;
};


// Set number of matrix rows and columns and 
// initialize matrix elements with a given value
template<class T>
void MatrixClass<T>::Resize(int numRows, int numCols, const T &value)
{
  a_.resize(numRows);
  for (int i=0;i<a_.size();++i)
  {
    a_[i].resize(numCols);
    for (int j=0;j<a_[i].size();++j)
        a_[i][j]=value;
  }
  numRows_=numRows;
  numCols_=numCols;
}

// Access matrix element at position (i,j)
template<class T>
T &MatrixClass<T>::operator()(int i, int j)
{
    if ((i<0)||(i>=numRows_))
    {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is (0:" << numRows_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    if ((j<0)||(j>=numCols_))
    {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is (0:" << numCols_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    return a_[i][j];
}

template<class T>
T MatrixClass<T>::operator()(int i, int j) const
{
  if ((i<0)||(i>=numRows_))
  {
    std::cerr << "Illegal row index " << i;
    std::cerr << " valid range is (0:" << numRows_-1 << ")";
    std::cerr << std::endl;
    exit(EXIT_FAILURE);
  }
  if ((j<0)||(j>=numCols_))
  {
    std::cerr << "Illegal column index " << j;
    std::cerr << " valid range is (0:" << numCols_-1 << ")";
    std::cerr << std::endl;
    exit(EXIT_FAILURE);
  }
  return a_[i][j];
}

template<class T>
void MatrixClass<T>::Print() const
{
    std::cout << "(" << numRows_ << "x";
    std::cout << numCols_ << ") matrix:" << std::endl;
    for (ConstRowIterator crowit=this->begin(); crowit!=this->end(); ++crowit)
    {
        std::cout << std::setprecision(3);
        for (ConstColIterator ccolit=crowit->begin(); ccolit!=crowit->end(); ++ccolit)
            std::cout << std::setw(5) << *ccolit << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}
