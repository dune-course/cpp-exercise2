/**
 *  Numeric matrices with type promoting operator+ 
 */

#include "nonnummatrix.h"

// starting with the basic template
// TODO: you have to modify the empty traits classes
template<class T1, class T2>
struct PromotionTraits
{};

template<class T>
class NumMatrixClass : public MatrixClass<T>
{
public:
    // Multiplication by a value x
    NumMatrixClass &operator*=(T x);
    
    // Addition with another matrix
    template<class T2>
    NumMatrixClass<typename PromotionTraits<T,T2>::promoted_type>&
        operator+=(const NumMatrixClass<T2> &x);
    
    // Constructors
    NumMatrixClass(int numRows=0, int numCols=0, const T &value=T()) : 
        MatrixClass<T>(numRows,numCols,value)
    {}
};

// Multiplication by a value x
template<class T>
NumMatrixClass<T> &NumMatrixClass<T>::operator*=(T x)
{
    for (int i=0;i<this->numRows_;++i)
        for (int j=0;j<this->numCols_;++j)
            this->a_[i][j]*=x;
    return *this;
}

// Addition with another matrix using type promotion
template<class T>
template<class T2>
NumMatrixClass<typename PromotionTraits<T,T2>::promoted_type>&
    NumMatrixClass<T>::operator+=(const NumMatrixClass<T2> &x)
{
    if ((x.Rows()!=this->numRows_)||(x.Cols()!=this->numCols_))
    {
        std::cerr << "Dimensions of matrix a (" << this->numRows_
                  << "x" << this->numCols_ << ") and matrix x (" 
                  << this->numRows_ << "x" << this->numCols_ << ") do not match!";
        exit(EXIT_FAILURE);
    }
    for (int i=0;i<this->numRows_;++i)
        for (int j=0;j<x.Cols();++j)
            this->a_[i][j]+=x(i,j);
    return *this;
}

// multiplication with other matrices or scalar types
template<class T>
NumMatrixClass<T> operator*(const NumMatrixClass<T> &a, T x)
{
    NumMatrixClass<T> temp(a);
    temp *= x;
    return temp;
}

template<class T>
NumMatrixClass<T> operator*(T x, const NumMatrixClass<T> &a)
{
    NumMatrixClass<T> temp(a);
    temp *= x;
    return temp;
}

template<class T1, class T2>
NumMatrixClass<typename PromotionTraits<T1,T2>::promoted_type> operator+(
  const NumMatrixClass<T1>&a, const NumMatrixClass<T2>&b)
{
  //TODO Implementation here
}  
